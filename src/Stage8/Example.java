package Stage8;

import java.util.ArrayList;

public class Example {
    public static void main(String args[]){

        Knight empress = new Knight("Empress", 41, Weapons.Blade);
        Knight kouda = new Knight("KouDa", 23, Weapons.Sword);
        Player noob = new Player("Noob");
        Player noname = new Player(72);

        ArrayList<Player> players = new ArrayList<>();

        players.add(empress);
        players.add(kouda);
        players.add(noob);
        players.add(noname);

        for(Player player : players){
            System.out.println(player.getStatus() + ". Твой класс: " + player.getClass().toString().substring(13));
        }

        System.out.println();

        for (int i = 0; i < players.size() - 1; i++){
            for (int k = i + 1; k < players.size(); k++){
                System.out.println(players.get(i).battle(players.get(k)));
            }
        }

        empress.upgrade();
        kouda.upgrade();

        System.out.println();

        for(Player player : players){
            System.out.println(player.getStatus() + ". Твой класс: " + player.getClass().toString().substring(13));
        }

        System.out.println();

        for (int i = 0; i < players.size() - 1; i++){
            for (int k = i + 1; k < players.size(); k++){
                System.out.println(players.get(i).battle(players.get(k)));
            }
        }


    }
}

class Player{


    private String nickname;

    private int forge;

    public Player(){
        forge = 1;
        nickname = "Неизвестно";
    }

    public Player(int forge){
        this.forge = forge;
        nickname = "Неизвестно";
    }

    public Player(String nickname){
        forge = 1;
        this.nickname = nickname;
    }

    public Player(String nickname, int forge){
        this.forge = forge;
        this.nickname = nickname;
    }

    public int getForge(){
        return forge;
    }

    public void setForge(int forge){
        this.forge = forge;
    }

    public String getStatus(){
        return ("Твой ник: " + nickname + ". Твоя сила: " + forge);
    }


    public String battle(Player player){
        if(forge > player.forge)
            return "Битва " + nickname + " c " + player.nickname + ". Победа " + nickname;
        else
            return "Битва " + nickname + " c " + player.nickname + ". " + nickname + " проиграл";
    }

}

class Knight extends Player{

    private int weapon_forge;

    public Knight(Weapons weapon){
        super();
        this.weapon_forge = weapon.getForge();
    }

    public Knight(int forge, Weapons weapon){
        super(forge);
        this.weapon_forge = weapon.getForge();
    }

    public Knight(String nickname, Weapons weapon){
        super(nickname);
        this.weapon_forge = weapon.getForge();
    }

    public Knight(String nickname, int forge, Weapons weapon){
        super(nickname, forge);
        this.weapon_forge = weapon.getForge();
    }

    public void upgrade(){
        setForge(getForge() + weapon_forge);
    }

}

enum Weapons {
    Sword(13), Blade(7);

    private int forge;

    Weapons(int forge){
        this.forge = forge;
    }

    int getForge(){
        return forge;
    }
}
package Stage12;

enum Gun {
    Usp(12), Glock(20), M4A4(30), AK(30), AWP(5);

    private int bullet;

    Gun(int b) {
        bullet = b;
    }

    int getAmountBullet(){
        return bullet;
    }
}

package Stage12;

public class Example_6 {
    public static void main(String[] args) {

        Integer myInt1 = new Integer(5);
        Character myChar1 = new Character('T');
        Boolean myBool1 = new Boolean(true);
        int myInt2 = myInt1.intValue();
        char myChar2 = myChar1.charValue();
        boolean myBool2 = myBool1.booleanValue();


        System.out.printf("%d = %d\n", myInt1, myInt2);
        System.out.printf("%c = %c\n", myChar1, myChar2);
        System.out.printf("%s = %s\n", myBool1, myBool2);

    }
}
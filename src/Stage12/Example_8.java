package Stage12;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Reload(gun = "AK-47", onReaload = false)
@Bullet(bullet = 30)

public class Example_8 {

    public static void main(String[] args) {

        meth();

    }
    @Reload(gun = "AWP", onReaload = true)
    @Bullet(bullet = 5)

    public static void meth(){

        Example_8 ex = new Example_8();

        try{

            for(Annotation a : ex.getClass().getAnnotations()){
                System.out.println(a);
            }

            System.out.println();

            Class<?> c = ex.getClass();
            Method m = c.getMethod("meth");
            Reload reload = m.getAnnotation(Reload.class);
            Bullet bullet = m.getAnnotation(Bullet.class);
            Reload reload2 = ex.getClass().getAnnotation(Reload.class);
            Bullet bullet2 = ex.getClass().getAnnotation(Bullet.class);

            for(Annotation a : m.getAnnotations()){
                System.out.println(a);
            }

            System.out.println();

            if(reload.onReaload())
                System.out.println(reload.gun() + " перезаряжен. " + bullet.bullet() + " патронов");
            else
                System.out.println(reload.gun() + " не перезаряжен");

            if(reload2.onReaload())
                System.out.println(reload2.gun() + " перезаряжен. " + bullet2.bullet());
            else
                System.out.println(reload2.gun() + " не перезаряжен");
        }
        catch (NoSuchMethodException e) {
            System.out.println("Нету");
        }
    }
}


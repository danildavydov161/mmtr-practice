package Stage12;

public class Example_4 {
    public static void main(String[] args) {
        Gun guns[] = Gun.values();
        for(Gun gun : guns){
            System.out.println(gun + " " + gun.ordinal());
        }

        if(guns[0].compareTo(guns[1]) < 0)
            System.out.printf("%s слабее %s\n", guns[0], guns[1]);
        if(guns[3].compareTo(guns[2]) > 0)
            System.out.printf("%s сильнее %s\n", guns[3], guns[2]);
        if(guns[4].compareTo(guns[4]) == 0)
            System.out.printf("%s - это %s\n", guns[4], guns[4]);
    }
}


package Stage12;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

public class Example_7 {

    public static void main(String[] args) {

        meth();

    }
    @Reload(gun = "ак-47", onReaload = false)

    public static void meth(){

        Example_8 ex = new Example_8();

        try{
            Class<?> c = ex.getClass();
            Method m = c.getMethod("meth");
            Reload reload = m.getAnnotation(Reload.class);
            if(reload.onReaload())
                System.out.println(reload.gun() + " перезаряжен");
            else
                System.out.println(reload.gun() + " не перезаряжен");
        }
        catch (NoSuchMethodException e) {
            System.out.println("Нету");
        }
    }
}


package Stage12;

import java.util.Random;

public class Example_5 {
    public static void main(String[] args) {

        System.out.println("Случайное оружие: " + gun());
        System.out.println("Случайное оружие: " + gun());
        System.out.println("Случайное оружие: " + gun());
        System.out.println("Случайное оружие: " + gun());
        System.out.println("Случайное оружие: " + gun());

    }

    static Gun gun(){
        Random rand = new Random();
        int chance = (int)(100 * rand.nextDouble());

        if(chance < 20)
            return Gun.Usp;
        else if(chance < 40)
            return Gun.Glock;
        else if(chance < 60)
            return Gun.M4A4;
        else if(chance < 80)
            return Gun.AK;
        else
            return Gun.AWP;
    }

}


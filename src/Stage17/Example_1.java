package Stage17;

public class Example_1 {
    public static void main(String[] args) {
        int num = 5213;
        System.out.println("10-чная СС: " + num);
        System.out.println("2-чная СС: " + Integer.toBinaryString(num));
        System.out.println("8-чная СС: " + Integer.toOctalString(num));
        System.out.println("16-чная СС: " + Integer.toHexString(num));
    }
}

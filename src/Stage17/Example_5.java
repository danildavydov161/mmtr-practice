package Stage17;

public class Example_5 {
    public static void main(String[] args) {
        long start, end;

        start = System.currentTimeMillis();
        System.out.println("Время старта программы: " + start);

        for (int i = 0; i < 100000000L; i++);

        end = System.currentTimeMillis();
        System.out.println("Время окончания программы: " + end);
        System.out.println("Скорость выполнения: " + (end - start));
    }
}

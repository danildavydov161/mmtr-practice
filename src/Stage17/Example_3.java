package Stage17;

public class Example_3 {
    public static void main(String[] args) {
        Runtime r = Runtime.getRuntime();
        Integer mas[] = new Integer[1000];
        long mem1, mem2;
        System.out.println("Памяти доступно: " + r.totalMemory());
        mem1 = r.freeMemory();
        System.out.println("Свобоной памяти: " + mem1);
        r.gc();
        mem1 = r.freeMemory();
        System.out.println("Свобоной памяти после очистки: " + mem1);

        for (int i = 0; i < mas.length; i++)
            mas[i] = i;

        mem2 = r.freeMemory();
        System.out.println("Свобоной памяти после выделения памяти для массива: " + r.freeMemory());
        System.out.println("Использовано для выделения памяти: " + (mem1 - mem2));

        for (int i = 0; i < 1000; i++) {
            mas[i] = null;
        }

        r.gc();
        mem1 = r.freeMemory();
        System.out.println("Свобоной памяти после очистки: " + mem1);
    }
}

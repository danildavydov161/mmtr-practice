package Stage17;

public class Example_2 {
    public static void main(String[] args) {

        char[] chars = {'B', '1', 't', '?', ' '};

        System.out.print("Символ:\t\t");
        for (char c : chars){
            System.out.print(c + "\t\t");
        }
        System.out.print("\nЦифра?\t\t");
        for (char c : chars){
            System.out.print(Character.isDigit(c) + "\t");
        }
        System.out.print("\nБуква?\t\t");
        for (char c : chars){
            System.out.print(Character.isAlphabetic(c) + "\t");
        }
        System.out.print("\nПробел?\t\t");
        for (char c : chars){
            System.out.print(Character.isWhitespace(c) + "\t");
        }
        System.out.print("\nПропись?\t");
        for (char c : chars){
            System.out.print(Character.isUpperCase(c) + "\t");
        }



    }
}

package Stage14;

public class Example {
    public static void main(String args[]){

       Gun<Pistol> usp = new Gun<>(new Pistol());
       Gun<Sniper> awp = new Gun<>(new Sniper());

       System.out.println("Оружие - " + usp.getGun().getClass().toString().replace("class Stage14.", ""));
       System.out.println(usp.getGun().putSilencer());
       System.out.println(usp.getGun().removeSilencer());

       System.out.println();

       System.out.println("Оружие - " + awp.getGun().getClass().toString().replace("class Stage14.", ""));
       System.out.println(awp.getGun().onZoom());
       System.out.println(awp.getGun().offZoom());

    }
}

class Gun<T>{

    private T gun;
    Gun(T gun){
        this.gun = gun;
    }
    public T getGun(){
        return gun;
    }
}

class Pistol{
    public String putSilencer(){
        return "Глушитель надет";
    }

    public String removeSilencer(){
        return "Глушитель снят";
    }
}

class Sniper{
    public String onZoom(){
        return "Включен прицел";
    }

    public String offZoom(){
        return "Выключен прицел";
    }
}

package Stage9;

public class Example {
    public static void main(String args[]){

        MallardDuck mallardDuck = new MallardDuck();
        RubberDuck rubberDuck = new RubberDuck();

        Object ducks[] = new Object[]{mallardDuck, rubberDuck};

        for (var duck : ducks)
        {
            System.out.println("Утка " + duck.getClass().toString().replace("class Stage9.", ""));

            if (duck instanceof Quackable)
            {
                System.out.println(((Quackable) duck).quack().replace("class Stage9.", ""));
            }

            if (duck instanceof Flyable)
            {
                System.out.println(((Flyable) duck).fly().replace("class Stage9.", ""));
            }
            else
                System.out.println(duck.getClass().toString().replace("class Stage9.", "") + " летать не может");

            System.out.println();
        }

    }
}

interface Flyable
{
    String fly();
}

interface Quackable
{
    String quack();
}
class MallardDuck implements Quackable, Flyable {

    public String quack()
    {
        return "*Кря* " + this.getClass();
    }

    public String fly()
    {
        return "*Звук полёта* " + this.getClass();
    }
}

class RubberDuck implements Quackable {

    public String quack()
    {
        return "*Пик* " + this.getClass();
    }
}
package Stage18;

import java.util.ArrayList;
import java.util.LinkedList;

public class Example_6 {
    public static void main(String args[]){
        ArrayList<String> array = new ArrayList<>();

        array.add("Боба");
        array.add("Биба");
        array.add("Буба");
        array.add("Лупа");
        array.add("Пупа");
        array.add(4, "Дэб");

        array.forEach(el -> System.out.print(el + " "));

        System.out.println();

        for(String el : array){
            el += "+ ";
            System.out.print(el);
        }

        System.out.println();



    }
}

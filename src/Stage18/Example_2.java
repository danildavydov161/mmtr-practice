package Stage18;

import java.util.*;

public class Example_2 {
    public static void main(String args[]){
        HashSet<String> array = new HashSet<>();

        array.add("Боба");
        array.add("Биба");
        array.add("Буба");
        array.add("Лупа");
        array.add("Пупа");

        System.out.println("HashSet: " + array);
        System.out.println("Его размер: " + array.size());

        array.remove("Буба");
        array.remove(2);

        System.out.println("HashSet после удаления элементов: " + array);
        System.out.println("Его размер: " + array.size());

    }
}

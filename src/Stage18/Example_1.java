package Stage18;

import java.util.*;

public class Example_1 {
    public static void main(String args[]){
        ArrayList<String> array = new ArrayList<>();
        LinkedList<String> list = new LinkedList<>();

        array.add("Боба");
        array.add("Биба");
        array.add("Буба");
        array.add("Лупа");
        array.add("Пупа");
        array.add(4, "Дэб");

        for(int i = 0; i< array.size(); i++)
            list.add(array.get(i));

        list.addLast("Моб");
        list.addFirst("Дуб");

        System.out.println("ArrayList: " + array);
        System.out.println("Его размер: " + array.size());

        System.out.println("LinkedList: " + list);

        array.remove("Буба");
        array.remove(2);
        list.removeFirst();
        list.removeLast();

        System.out.println("ArrayList после удаления элементов: " + array);
        System.out.println("Его размер: " + array.size());

        System.out.println("LinkedList после удаления элементов: " + list);
    }
}

package Stage18;

import java.util.*;

public class Example_5 {
    public static void main(String args[]){
        ArrayList<String> list = new ArrayList<>();

        list.add("Боба");
        list.add("Биба");
        list.add("Буба");
        list.add("Лупа");
        list.add("Пупа");
        list.add(4, "Дэб");

        Iterator<String> it = list.iterator();

        while (it.hasNext()){
            System.out.print(it.next() + " ");
        }

        System.out.println();

        ListIterator<String> listIt = list.listIterator();

        while(listIt.hasNext()){
            listIt.set(listIt.next() + "+");
        }

        it = list.iterator();

        while (it.hasNext()){
            System.out.print(it.next() + " ");
        }

        System.out.println();

        while (listIt.hasPrevious()){
            System.out.print(listIt.previous() + " ");
        }

    }
}

package Stage18;

import java.util.ArrayList;
import java.util.Spliterator;

public class Example_7 {
    public static void main(String args[]){
        ArrayList<String> array = new ArrayList<>();
        ArrayList<String> array2 = new ArrayList<>();

        array.add("Боба");
        array.add("Биба");
        array.add("Буба");
        array.add("Лупа");
        array.add("Пупа");
        array.add(4, "Дэб");

        Spliterator<String> split = array.spliterator();

        while (split.tryAdvance(el -> System.out.print(el + " ")));

        System.out.println();

        split = array.spliterator();

        while (split.tryAdvance(el -> array2.add(el + "+")));

        split = array2.spliterator();

        while (split.tryAdvance(el -> System.out.print(el + " ")));

    }
}

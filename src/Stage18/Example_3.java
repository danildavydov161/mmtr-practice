package Stage18;

import java.util.*;

public class Example_3 {
    public static void main(String args[]){
        TreeSet<String> array = new TreeSet<>();

        array.add("Ф");
        array.add("Ы");
        array.add("А");
        array.add("П");
        array.add("Р");

        System.out.println("TreeSet: " + array);
        System.out.println(array.subSet("П", "Ы"));


    }
}

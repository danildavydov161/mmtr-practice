package Stage18;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Example_8 {
    public static void main(String args[]){
        HashMap<String, Integer> list = new HashMap<>();

        list.put("Боба", 15);
        list.put("Биба", 18);
        list.put("Буба", 25);
        list.put("Лупа", 13);
        list.put("Пупа", 31);

        for (Map.Entry<String, Integer> el : list.entrySet()){
            System.out.println(el.getKey() + ": возраст - " + el.getValue());
        }

    }
}

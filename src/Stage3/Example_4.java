package Stage3;

public class Example_4 {
    public static void main(String[] args) {

        Boolean bool1, bool2 = true;
        int a = 5, b = 4;
        if(a > b){
            bool1 = true;
            System.out.println(bool1);
        }
        if(bool2){
            System.out.println("a > b");
        }
        a = 3;
        if(a < b){
            bool1 = false;
            bool2 = false;
            System.out.println(bool1);
        }
        if(bool2){
            System.out.println("a > b");
        } //не выполняется, т.к. bool2 = false
    }
}
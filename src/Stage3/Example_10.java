package Stage3;

public class Example_10 {
    public static void main(String[] args) {


        int mas[][] = new int[5][5];

        for (int i = 0; i < mas.length; i++){
            for (int k = 0; k < mas.length; k++){
                if(i == k || i + k == 4){
                    mas[i][k] = 0;
                }
                else{
                    mas[i][k] = 1;
                }
            }
        }

        for (int i = 0; i < mas.length; i++){
            for (int k = 0; k < mas.length; k++){
                System.out.print(mas[i][k] + " ");
            }
            System.out.println();
        }


    }
}
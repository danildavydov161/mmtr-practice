package Stage3;

public class Example_9 {
    public static void main(String[] args) {


        int mas1[] = new int[5];
        int mas2[] = new int[]{4, 21, 5, 12, 34};

        System.out.println("Массив №1:");
        for (int i = 0; i < mas1.length; i++){
            mas1[i] = i * 3 + 5;
            System.out.print(mas1[i] + " ");
        }

        System.out.println("\n----------");
        System.out.println("Массив №1 + Массив №2:");

        for (int i = 0; i < mas1.length; i++){
            mas1[i] += mas2[i];
            System.out.print(mas1[i] + " ");
        }

    }
}
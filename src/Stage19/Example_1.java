package Stage19;

import java.util.StringTokenizer;

public class Example_1 {
    public static void main(String args[]) {

        String str = "Ты1кто1будешь?";

        StringTokenizer st = new StringTokenizer(str, "1"); // Прикольня штука - разделитель строк.
        while (st.hasMoreTokens()) {
            String str1 = st.nextToken();
            String str2 = st.nextToken();
            String str3 = st.nextToken();
            System.out.println(str1 + " " + str2 + " " + str3);
        }
    }
}

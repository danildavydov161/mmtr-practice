package Stage19;

import java.util.*;

public class Example_2 {
    public static void main(String args[]) {

        BitSet bit1 = new BitSet(16);
        BitSet bit2 = new BitSet(16);

        for(int i = 0; i < 16; i++){
            if(i % 2 == 0) bit1.set(i);
            if(i % 3 != 0) bit2.set(i);
        }

        System.out.println("BitSet 1: " + bit1);
        System.out.println("BitSet 2: " + bit2);

        bit2.and(bit1);

        System.out.println("Операция И: " + bit2);

        bit2.or(bit1);

        System.out.println("Операция ИЛИ: " + bit2);

        bit2.xor(bit1);

        System.out.println("Операция, исключающая ИЛИ: " + bit2);

    }
}

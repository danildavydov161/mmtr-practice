package Stage6_7;

import java.util.ArrayList;

public class Example {
    public static void main(String args[]){

        Player empress = new Player("Empress", 41);
        Player kouda = new Player("KouDa", 23);
        Player noob = new Player("Noob");
        Player noname = new Player(72);

        ArrayList<Player> players = new ArrayList<>();

        players.add(empress);
        players.add(kouda);
        players.add(noob);
        players.add(noname);

        for(Player player : players){
            System.out.println(player.getStatus());
        }

        System.out.println();

        for (int i = 0; i < players.size() - 1; i++){
            for (int k = i + 1; k < players.size(); k++){
                System.out.println(players.get(i).battle(players.get(k)));
            }
        }

        System.out.println();

        empress.upgrade(Weapons.Sword);
        kouda.upgrade(Weapons.Blade, Weapons.Sword);
        noob.upgrade(Weapons.Blade, Weapons.Blade, Weapons. Armor);

        for(Player player : players){
            System.out.println(player.getStatus());
        }

        System.out.println();

        for (int i = 0; i < players.size() - 1; i++){
            for (int k = i + 1; k < players.size(); k++){
                System.out.println(players.get(i).battle(players.get(k)));
            }
        }


    }
}

class Player{


    String nickname;

    int forge;

    public Player(){
        forge = 1;
        nickname = "Неизвестно";
    }

    public Player(int forge){
        this.forge = forge;
        nickname = "Неизвестно";
    }

    public Player(String nickname){
        forge = 1;
        this.nickname = nickname;
    }

    public Player(String nickname, int forge){
        this.forge = forge;
        this.nickname = nickname;
    }

    public String getStatus(){
        return ("Твой ник: " + nickname + ". Твоя сила: " + forge);
    }

    public void upgrade(Weapons weapon){
        forge += weapon.getForge();
    }

    public void upgrade(Weapons weapon1, Weapons weapon2){
        forge += (weapon1.getForge() + weapon2.forge);
    }

    public void upgrade(Weapons weapon1, Weapons weapon2, Weapons weapon3){
        forge += (weapon1.getForge() + weapon2.forge + weapon3.forge);
    }

    public String battle(Player player){
        if(forge > player.forge)
            return "Битва " + nickname + " c " + player.nickname + ". Победа " + nickname;
        else
            return "Битва " + nickname + " c " + player.nickname + ". " + nickname + " проиграл";
    }

}

enum Weapons{
    Sword(13), Blade(7), Armor(10);

    int forge;

    Weapons(int forge){
        this.forge = forge;
    }

    int getForge(){
        return forge;
    }
}